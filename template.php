<?php
function buz1n_process_node(&$vars) {
	// Change default text of the read more link.
	if (isset($vars['links']['node']['#links']['node-readmore'])) {
		$vars['links']['node']['#links']['node-readmore']['title'] = t('Leer mas');
	}
}
?>